import { Button, Box, Grid, Typography, useMediaQuery, useTheme } from "@material-ui/core";
import { withStyles, Theme } from "@material-ui/core/styles";
import React, { useState } from "react";
import Footer from "../src/components/Footer";
import DownloadDialog from "../src/components/FormDialog";
import Header from "../src/components/Header";
import PricingCard from "../src/components/PricingCard";

const ColorButton = withStyles((theme: Theme) => ({
  root: {
    borderColor: theme.palette.primary.main,
    borderWidth: "2px",
    padding: "16px 32px",
  },
}))(Button);

const HeroButton = (p) => <ColorButton variant="outlined" size="large" {...p} />;
const MediaCenters = [{ id: "plex" }, { id: "emby" }, { id: "kodi" }];

export default function Index() {
  const theme = useTheme();
  const isDownSm = useMediaQuery(theme.breakpoints.down("sm"));
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <>
      <DownloadDialog open={open} onClose={handleClose} />

      <Header />

      <Box mt={4}>
        <Grid container spacing={6} direction={isDownSm ? "column-reverse" : "row"}>
          <Grid item md={6} xs={12}>
            <Box display="flex" flexDirection="column" justifyContent="space-around" height="100%">
              <Typography variant="h2" paragraph gutterBottom>
                The perfect home for your media.
              </Typography>
              <Typography variant="subtitle1" paragraph gutterBottom>
                Enjoy super-cheap storage for your media files, completely under your control. SiaStream uses next-gen
                cloud storage technology to provide fast streaming at the lowest costs. Optimized for Plex.
              </Typography>
              <Box mt={2}>
                <HeroButton onClick={handleOpen}>Download SiaStream v1.0.4</HeroButton>
              </Box>
            </Box>
          </Grid>
          <Grid item md={6} xs={12}>
            <Box display="flex" justifyContent="center" alignItems="center" height="100%">
              <img src={require("../src/images/hero.png")} style={{ maxHeight: 300 }} />
            </Box>
          </Grid>
        </Grid>
      </Box>

      <Box mt={isDownSm ? 4 : 16}>
        <Grid container spacing={6} direction={isDownSm ? "column" : "row"}>
          <Grid item md={6} xs={12}>
            <Box display="flex" justifyContent="center" alignItems="center" height="100%">
              <img src={require("../src/images/hosting.png")} style={{ maxHeight: 300 }} />
            </Box>
          </Grid>
          <Grid item md={6} xs={12}>
            <Typography variant="overline">Meet SiaStream</Typography>

            <Typography variant="h3" gutterBottom>
              True Cloud Freedom
            </Typography>

            <Typography variant="body1" paragraph>
              Replace your existing storage solution for your media files. Store up to 35 TB on SiaStream, mount as a
              FUSE drive, and stream files directly to your Plex or other media software.
            </Typography>

            <Typography variant="body1" paragraph>
              SiaStream is the first cloud streaming solution that is fully under your control. Never again worry about
              being deplatformed.
            </Typography>

            <Box display="flex" mt={2}>
              {MediaCenters.map((mc) => (
                <Box key={mc.id} mr={4}>
                  <img src={require(`../src/images/${mc.id}.png`)} style={{ height: 60, width: 60 }} />
                </Box>
              ))}
            </Box>
          </Grid>
        </Grid>
      </Box>

      <Box mt={isDownSm ? 4 : 16}>
        <Grid container spacing={6} direction={isDownSm ? "column-reverse" : "row"}>
          <Grid item md={6} xs={12}>
            <Box display="flex" flexDirection="column" justifyContent="space-around" height="100%">
              <Typography variant="overline">Encrypted, Redundant, Secure</Typography>
              <Typography variant="h3" gutterBottom>
                Next-Gen Cloud Storage
              </Typography>
              <Typography variant="body1" paragraph>
                SiaStream uses the Sia network, a next-gen cloud storage network that splits up your data, encrypts it,
                and stores it on a worldwide network of providers.
              </Typography>
              <Typography variant="body1" paragraph>
                Sia is completely decentralized, meaning there are no signups, no servers, and no trusted third parties.
                Your files are private and under your control.
              </Typography>
              <Box mt={2}>
                <HeroButton href="https://sia.tech/technology" target="_blank">
                  How Sia Works
                </HeroButton>
              </Box>
            </Box>
          </Grid>
          <Grid item md={6} xs={12}>
            <Box display="flex" justifyContent="center" alignItems="center" height="100%">
              <img src={require("../src/images/globe.png")} style={{ maxHeight: 300 }} />
            </Box>
          </Grid>
        </Grid>
      </Box>

      <Box mt={isDownSm ? 4 : 16}>
        <Grid container spacing={6}>
          <Grid item md={6} xs={12}>
            <Box display="flex" flexDirection="column" justifyContent="space-around" height="100%">
              <PricingCard />
            </Box>
          </Grid>
          <Grid item md={6} xs={12}>
            <Typography variant="overline">Ultra Competitive Pricing</Typography>
            <Typography variant="h3" gutterBottom>
              Marketplace Magic
            </Typography>
            <Typography variant="body1" paragraph>
              Sia is a global marketplace for cloud storage, where providers compete for your business. This means
              better price efficiency, resulting in lower storage costs than any other competing service-provider – by
              an order of magnitude.
            </Typography>
          </Grid>
        </Grid>
      </Box>

      <Box mt={isDownSm ? 4 : 16}>
        <Box display="flex" alignItems="center" justifyContent="center" flexDirection="column">
          <Typography variant="h2" style={{ fontSize: isDownSm ? "1.5rem" : "2.5rem" }}>
            always open source.
          </Typography>
          <Box mt={4}>
            <HeroButton onClick={handleOpen}>Download SiaStream v1.0.4</HeroButton>
          </Box>
        </Box>
      </Box>

      <Box mt={isDownSm ? 4 : 16}>
        <Footer />
      </Box>
    </>
  );
}
