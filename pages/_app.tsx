import { createMuiTheme, Container, CssBaseline } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/styles";
import App from "next/app";
import { SnackbarProvider } from "notistack";
import React from "react";
import "typeface-metropolis"; // import Metropolis typeface
import "./globals.css";

const generatedTheme = createMuiTheme({
  typography: {
    fontFamily: "Metropolis, system-ui, Sans-Serif",
    h1: {
      fontSize: "3.5rem",
      fontWeight: 300,
    },
    h2: {
      fontSize: "3.125rem",
      fontWeight: 400,
      lineHeight: 1.2,
    },
    h3: {
      fontSize: "2rem",
      fontWeight: 400,
      lineHeight: 1.4,
    },
    h4: {
      fontSize: 20,
      fontWeight: 800,
      lineHeight: 1.4,
    },
    subtitle1: {
      fontSize: "1.25rem",
      lineHeight: 1.4,
    },
    overline: {
      fontSize: "1rem",
      color: "#1db954",
      fontWeight: 600,
      letterSpacing: "2px",
    },
    body1: {
      fontSize: "1.125rem",
      lineHeight: 1.6,
    },
    button: {
      textTransform: "none",
      fontWeight: 600,
    },
  },
  palette: {
    type: "dark",
    primary: { main: "#1db954" },
    background: { default: "#171717", paper: "#1F1F1F" },
  },
  overrides: {
    MuiLink: {
      root: {
        fontSize: "1.125rem",
        lineHeight: 1.6,
      },
    },
  },
});

generatedTheme.typography.h2 = {
  ...generatedTheme.typography.h2,
  [generatedTheme.breakpoints.down("md")]: {
    fontSize: "2.5rem",
  },
};

export const materialTheme = generatedTheme;

class MyApp extends App {
  // Only uncomment this method if you have blocking data requirements for
  // every single page in your application. This disables the ability to
  // perform automatic static optimization, causing every page in your app to
  // be server-side rendered.
  //
  // static async getInitialProps(appContext) {
  //   // calls page's `getInitialProps` and fills `appProps.pageProps`
  //   const appProps = await App.getInitialProps(appContext);
  //
  //   return { ...appProps }
  // }

  render() {
    const { Component, pageProps } = this.props;
    return (
      <ThemeProvider theme={materialTheme}>
        <CssBaseline />
        <SnackbarProvider anchorOrigin={{ vertical: "top", horizontal: "right" }}>
          <Container>
            <Component {...pageProps} />
          </Container>
        </SnackbarProvider>
      </ThemeProvider>
    );
  }
}

export default MyApp;
