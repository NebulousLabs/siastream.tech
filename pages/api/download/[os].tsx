import fs from "fs";
import path from "path";
import low from "lowdb";
import FileSync from "lowdb/adapters/FileSync";

const adapter = new FileSync("download-counter.json");
const db = low(adapter);

db.defaults({ linux: 0, macos: 0, total: 0 }).write();

export default (req, res) => {
  const os = req.query.os;
  const fileName = `siastream-${os}-x64.zip`;
  const filePath = path.resolve(".", `public/${fileName}`);
  const stat = fs.statSync(filePath);

  db.update(os, (count) => count + 1).write();
  db.update("total", (count) => count + 1).write();

  res.writeHead(200, {
    "Content-Disposition": `attachment;filename=${fileName}`,
    "Content-Type": "application/octet-stream",
    "Content-Length": stat.size,
  });

  const readStream = fs.createReadStream(filePath);

  readStream.pipe(res);
};
