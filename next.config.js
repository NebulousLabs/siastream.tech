const withOptimizedImages = require("next-optimized-images");
const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");

module.exports = withOptimizedImages({
  webpack: (config) => {
    if (config.resolve.plugins) {
      config.resolve.plugins.push(new TsconfigPathsPlugin());
    } else {
      config.resolve.plugins = [new TsconfigPathsPlugin()];
    }

    return config;
  },
});
