import { Box, Card, CardContent, Chip, Grid, Hidden, Typography } from "@material-ui/core";

const PricingCard = () => {
  return (
    <Card raised style={{ background: "linear-gradient(180deg, #3A9B55 0%, #25542A 100%)" }}>
      <CardContent>
        <Grid container spacing={2}>
          <Grid item md={6} sm={12}>
            <Box display="flex" height={140} alignItems="center">
              <Box display="flex" justifyContent="center" flexDirection="column">
                <Box style={{ fontWeight: 800, fontSize: 40 }} mb={1}>
                  $3.99/TB
                </Box>
                <Chip label="Sia Network" color="primary" style={{ color: "#fff", fontWeight: "bold" }} />
              </Box>
            </Box>
            <Typography variant="subtitle2">Per Month at 3x Redundancy.</Typography>
          </Grid>
          <Hidden smDown>
            <Grid item md={6}>
              <Box
                display="flex"
                alignItems="center"
                justifyContent="space-around"
                height="100%"
                style={{ opacity: 0.5 }}
              >
                <Box display="flex" justifyContent="center" flexDirection="column" textAlign="center">
                  <Box mb={1}>
                    <Typography variant="h4">$23/TB</Typography>
                  </Box>
                  <Chip label="Amazon S3" style={{ background: "#F7A500", fontWeight: "bold" }} />
                </Box>
                <Box display="flex" justifyContent="center" flexDirection="column" textAlign="center">
                  <Box mb={1}>
                    <Typography variant="h4">$20/TB</Typography>
                  </Box>
                  <Chip label="Google Cloud" style={{ background: "#6C52FE", fontWeight: "bold" }} />
                </Box>
              </Box>
            </Grid>
          </Hidden>
          <Grid item sm={12}>
            <Typography variant="subtitle2">
              SiaStream uses the Sia marketplace, storage prices can sometimes temporarily exceed $3.99/TB/month.
            </Typography>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

export default PricingCard;
