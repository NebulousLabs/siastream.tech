import {
  FormControlLabel,
  Button,
  Box,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Typography,
  Checkbox,
  Collapse,
  Tabs,
  Tab,
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  Link,
} from "@material-ui/core";
import { ChevronRight } from "@material-ui/icons";
import { useFormik } from "formik";
import { useSnackbar } from "notistack";
import React, { useState, useEffect } from "react";
import MailchimpSubscribe from "react-mailchimp-subscribe";
import * as Yup from "yup";

const url = "https://tech.us11.list-manage.com/subscribe/post?u=5df238d9e852f9801b5f2c92e&amp;id=e846980a05";

interface TabPanelProps {
  children?: React.ReactNode;
  dir?: string;
  index: any;
  value: any;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && <Box>{children}</Box>}
    </div>
  );
}

function FormDialog({ onClose, open, subscribe, status, message }) {
  const { enqueueSnackbar } = useSnackbar();
  const [isDownloadSectionOpened, setDownloadSectionOpened] = useState(false);
  const formik = useFormik({
    initialValues: {
      email: "",
      sia: false,
    },
    validationSchema: Yup.object({
      email: Yup.string().email("Email address must be valid").required("Email address is required"),
      sia: Yup.boolean(),
    }),
    onSubmit: async ({ email, sia }, { setSubmitting }) => {
      subscribe({ EMAIL: email, SIAUSER: sia ? "Yes" : "No" });
      setSubmitting(false);
      openDownloadSection();
    },
  });
  const openDownloadSection = () => {
    setDownloadSectionOpened(true);
  };
  const handleClose = () => {
    formik.resetForm();
    onClose();
    setDownloadSectionOpened(false);
  };
  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  useEffect(() => {
    if (status === "success") {
      enqueueSnackbar("Thank you for subscribing!", { variant: "success" });
    } else if (status === "error" && message.includes("is already subscribed")) {
      enqueueSnackbar("You are already subscribed!", { variant: "info" });
    } else if (status === "error") {
      enqueueSnackbar(message, { variant: "warning" });
    }
  }, [status, message]);

  return (
    <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title" maxWidth="md">
      <form onSubmit={formik.handleSubmit}>
        <DialogTitle id="form-dialog-title">
          <Typography variant="overline">Download SiaStream v1.0.4</Typography>
        </DialogTitle>
        <DialogContent>
          <Collapse in={!isDownloadSectionOpened}>
            <DialogContentText>
              Get early access to SiaStream news, Product updates, Exclusive content and Useful tips. Zero Spam,
              unsubscribe at any time.
            </DialogContentText>

            <Box mb={2}>
              <TextField
                autoFocus
                label="Enter your email address here"
                type="email"
                name="email"
                fullWidth
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.email && Boolean(formik.errors.email)}
                helperText={formik.touched.email && formik.errors.email}
                value={formik.values.email}
              />
            </Box>
            <Box mb={2}>
              <FormControlLabel
                control={
                  <Checkbox name="sia" color="primary" onChange={formik.handleChange} checked={formik.values.sia} />
                }
                label="I have previous experience using Sia"
              />
            </Box>
          </Collapse>
          <Collapse in={isDownloadSectionOpened}>
            <Box>
              <Tabs value={value} indicatorColor="primary" textColor="primary" onChange={handleChange}>
                <Tab label="Install via Homebrew" />
                <Tab label="Install via NPM" />
                <Tab label="Download binaries" />
              </Tabs>
              <TabPanel value={value} index={0}>
                <List>
                  <ListItem>
                    <ListItemIcon>
                      <ChevronRight />
                    </ListItemIcon>
                    <ListItemText
                      primary={
                        <>
                          Install{" "}
                          <Link href="https://brew.sh" target="_blank" rel="noopener noreferrer">
                            Homebrew
                          </Link>
                        </>
                      }
                      secondary="available for macos and linux"
                    />
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      <ChevronRight />
                    </ListItemIcon>
                    <ListItemText
                      primary="Add our custom tap"
                      secondary="brew tap NebulousLabs/siastream https://gitlab.com/NebulousLabs/siastream.git"
                    />
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      <ChevronRight />
                    </ListItemIcon>
                    <ListItemText primary="Install" secondary="brew install siastream" />
                  </ListItem>
                </List>
              </TabPanel>
              <TabPanel value={value} index={1}>
                <List>
                  <ListItem>
                    <ListItemIcon>
                      <ChevronRight />
                    </ListItemIcon>
                    <ListItemText
                      primary={
                        <>
                          Install{" "}
                          <Link href="https://nodejs.org" target="_blank" rel="noopener noreferrer">
                            NodeJS
                          </Link>
                        </>
                      }
                      secondary="available for macos and linux"
                    />
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      <ChevronRight />
                    </ListItemIcon>
                    <ListItemText primary="Install" secondary="npm i -g siastream" />
                  </ListItem>
                </List>
              </TabPanel>
              <TabPanel value={value} index={2}>
                <List>
                  <ListItem>
                    <ListItemIcon>
                      <ChevronRight />
                    </ListItemIcon>
                    <ListItemText
                      primary={<Link href="https://siastream.tech/api/download/macos">Download for MacOS</Link>}
                      secondary="v1.0.4"
                    />
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      <ChevronRight />
                    </ListItemIcon>
                    <ListItemText
                      primary={<Link href="https://siastream.tech/api/download/linux">Download for Linux</Link>}
                      secondary="v1.0.4"
                    />
                  </ListItem>
                </List>
              </TabPanel>
            </Box>
          </Collapse>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          {!isDownloadSectionOpened && (
            <Button type="button" onClick={openDownloadSection}>
              Download
            </Button>
          )}
          {!isDownloadSectionOpened && (
            <Button type="submit" color="primary">
              Subscribe &amp; Download
            </Button>
          )}
        </DialogActions>
      </form>
    </Dialog>
  );
}

export default function DownloadDialog({ onClose, open }) {
  return (
    <MailchimpSubscribe
      url={url}
      render={({ subscribe, status, message }) => (
        <FormDialog subscribe={subscribe} status={status} message={message} onClose={onClose} open={open} />
      )}
    />
  );
}
