import { List, Box, Link, Grid, Typography, ListItem } from "@material-ui/core";
import { Theme, makeStyles, createStyles } from "@material-ui/core/styles";
import React from "react";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    listItem: {
      [theme.breakpoints.down("sm")]: {
        justifyContent: "center",
      },
    },
  })
);

export default function Footer() {
  const classes = useStyles();

  return (
    <Grid container>
      <Grid item md={4} xs={12}>
        <Box display="flex" flexDirection="column" justifyContent="center" alignItems="center" height="100%">
          <img style={{ width: 80 }} src={require("../images/built-with-sia.png")} />
        </Box>
      </Grid>

      <Grid item md={4} xs={12}>
        <List>
          <ListItem className={classes.listItem}>
            <Typography variant="h5">Resources</Typography>
          </ListItem>
          <ListItem className={classes.listItem}>
            <Link href="https://sia.tech" target="_blank" rel="noopener noreferrer">
              Sia.tech
            </Link>
          </ListItem>

          <ListItem className={classes.listItem}>
            <Link href="https://support.siastream.tech" target="_blank" rel="noopener noreferrer">
              Support
            </Link>
          </ListItem>

          <ListItem className={classes.listItem}>
            <Link href="https://gitlab.com/NebulousLabs/siastream" target="_blank" rel="noopener noreferrer">
              Source Code
            </Link>
          </ListItem>
        </List>
      </Grid>

      <Grid item md={4} xs={12}>
        <List>
          <ListItem className={classes.listItem}>
            <Typography variant="h5">Community</Typography>
          </ListItem>

          <ListItem className={classes.listItem}>
            <Link href="https://twitter.com/siatechhq" target="_blank" rel="noopener noreferrer">
              Twitter
            </Link>
          </ListItem>

          <ListItem className={classes.listItem}>
            <Link href="https://discord.gg/sia" target="_blank" rel="noopener noreferrer">
              Discord
            </Link>
          </ListItem>

          <ListItem className={classes.listItem}>
            <Link href="https://www.reddit.com/r/siacoin" target="_blank" rel="noopener noreferrer">
              Reddit
            </Link>
          </ListItem>

          <ListItem className={classes.listItem}>
            <Link href="https://blog.sia.tech" target="_blank" rel="noopener noreferrer">
              Blog
            </Link>
          </ListItem>
        </List>
      </Grid>
    </Grid>
  );
}
