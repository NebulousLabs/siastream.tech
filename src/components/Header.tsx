import { Typography, Box, useTheme, Link, useMediaQuery } from "@material-ui/core";

export default function Header() {
  const theme = useTheme();
  const isDownSm = useMediaQuery(theme.breakpoints.down("sm"));
  return (
    <Box display="flex" alignItems="center" height={isDownSm ? 80 : 120} justifyContent="space-between">
      <Box display="flex" alignItems="center">
        <Box mr={2}>
          <img src={require("../images/logo.png")} style={{ width: 50 }} />
        </Box>
        <Typography>
          Sia<b>Stream</b>
        </Typography>
      </Box>
      <Link href="https://support.siastream.tech" target="_blank" rel="noopener noreferrer">
        Get Support
      </Link>
    </Box>
  );
}
